import { Entity, Spritesheet, App, EntitySettings, COLLISION_TYPE, Input } from 'squaredjs'
import AutomaticZOrdering from '../components/automaticZOrdering'

class Player extends Entity {

  public allowInput = true

  constructor(settings: EntitySettings) {
    super(settings)
    this.offset.x = 3
    this.offset.y = 8

    this.width = 10
    this.height = 8

    const tileset = new Spritesheet('assets/tilesets/0x72_16x16DungeonTileset.v4.png', 16, 16)
    const tile = tileset.tileCoordsToNumber(4, 8)
    this.addAnimation(tileset, 'idle', [[64, 128]], 1, { loop: true })

    // RigidBody settings
    this.body.collisionType = COLLISION_TYPE.ACTIVE
    this.body.friction.x = Number.MAX_SAFE_INTEGER
    this.body.friction.y = Number.MAX_SAFE_INTEGER
    this.body.maxVel = { x: 50, y: 50 }

    // Z-ordering component
    new AutomaticZOrdering(this)
  }

  public setVel(x: number, y: number): void {
    this.body.vel.x = x
    this.body.vel.y = y
  }

  public flipX(flip: boolean): void {
    this.flip.x = flip
  }

  public update(delta: number): void {
    super.update(delta)

    this.body.accel.x = 0
    this.body.accel.y = 0

    const accel = Number.MAX_SAFE_INTEGER
    if (this.allowInput) {
      if (App.instance.input.state('left')) {
        this.body.accel.x = -accel
        this.flipX(true)
      }
      if (App.instance.input.state('right')) {
        this.body.accel.x = accel
        this.flipX(false)
      }
      if (App.instance.input.state('up')) {
        this.body.accel.y = -accel
      }
      if (App.instance.input.state('down')) {
        this.body.accel.y = accel
      }
    }

    this.draw(delta)
  }

  private draw(delta: number): void {
    const x = Math.round(this.x)
    const y = Math.round(this.y)
    const distance = 20
    const hopHor = x % distance <= distance / 2

    this.sprite.y += hopHor ? -1 : 0
  }
}

export default Player
