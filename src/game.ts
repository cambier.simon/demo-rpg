import { App, Input } from 'squaredjs'
import RigidBody from '../node_modules/squaredjs/dist/components/rigidBody'
import * as TWEEN from '@tweenjs/tween.js'
import Entity from '../node_modules/squaredjs/dist/entity'

const debug = require('debug')('squared:game/main')

class Game extends App {

  private t: number = 0

  constructor() {
    super('game', { scale: 3, width: 16 * 16, height: 16 * 12 })

    this.input.bind('up', Input.KEY.UP_ARROW)
    this.input.bind('down', Input.KEY.DOWN_ARROW)
    this.input.bind('left', Input.KEY.LEFT_ARROW)
    this.input.bind('right', Input.KEY.RIGHT_ARROW)
    this.input.bind('jump', Input.KEY.SPACE)

    RigidBody.baseGravityValue = 0
  }

  public update(delta: number): void {
    super.update(delta)
    TWEEN.update()
    if (process.env.NODE_ENV === 'development') {
      for (const entity of Entity.entities) {
        entity.body.drawBox()
      }
    }
  }
}

const game = new Game()

game.prepare()
  .then(async () => {
    const level = require('./levels/dungeon1.json')
    await game.loadLevel(level)
  })
